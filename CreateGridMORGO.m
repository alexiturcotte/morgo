%
% MOPSO's CreateGrid: 
% ----------------------------------------------------------------------
% Copyright (c) 2015, Yarpiz (www.yarpiz.com)
% All rights reserved. Please read the "license.txt" for license terms.
%
% Developer: S. Mostapha Kalami Heris (Member of Yarpiz Team)
% 
% Contact Info: sm.kalami@gmail.com, info@yarpiz.com
%
% ----------------------------------------------------------------------
% MORGO's CreateGridMORGO:
% 
% Very minor modification to the Yarpiz MOPSO CreateGrid function. Here,
% we take the function evaluations as a parameter (fEvals) to avoid having
% to recompute them.
%
% The main difference between this and CreateGrid is purely structural, as
% the MOPSO code is built on a structure, and MORGO simply uses matrices.
%
% ----------------------------------------------------------------------

function Grid=CreateGridMORGO(fEvals,nGrid,alpha)

    c=fEvals;
    
    cmin=min(c,[],2);
    cmax=max(c,[],2);
    
    dc=cmax-cmin;
    cmin=cmin-alpha*dc;
    cmax=cmax+alpha*dc;
    
    nObj=size(c,1);
    
    empty_grid.LB=[];
    empty_grid.UB=[];
    Grid=repmat(empty_grid,nObj,1);
    
    for j=1:nObj
        
        cj=linspace(cmin(j),cmax(j),nGrid+1);
        
        Grid(j).LB=[-inf cj];
        Grid(j).UB=[cj +inf];
        
    end

end