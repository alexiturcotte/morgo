%
% Yang's Bat Algorithm for Single Objective Optimization
%
% Parameters:
%
%   <> para             optional array of parameters
%   <> Fun              the function to optimize
%   <> d                the dimension of the independent variable
%   <> lb               lower bound of constraint hypercube
%   <> ub               upper bound of constraint hypercube
%   <> innerNumIters    max number of iterations
%   <> initOptions      optional initialization parameter
%
% The only modification we made to this algorithm is the added
% consideration of constraints.
%
function [best,fmin,N_iter] = bat_algorithm(para, Fun, d, lb, ub, innerNumIters, initOption)

    % Default parameters
    if length( para) ~= 3,  para = [10 0.25 0.5];  end
    n=para(1);      % Population size, typically 10 to 25
    A=para(2);      % Loudness  (constant or decreasing)
    r=para(3);      % Pulse rate (constant or decreasing)
    
    % This frequency range determines the scalings
    Qmin=0;         % Frequency minimum
    Qmax=2;         % Frequency maximum
    
    % Iteration parameters
    % In order to obtain better/more accurate results, N_iter
    % should be increased to N_iter=2000 or more if necessary.
    N_iter=innerNumIters;       % Total number of function evaluations
    
    % Dimension of the search variables
    % d=3;
    
    % Initial arrays
    Q=zeros(n,1);   % Frequency
    v=zeros(n,d);   % Velocities
    
    % Scale of consideration defined as the upper bound - lower bound
    scale = ub - lb;
    
    % Initialize the population/solutions
    if strcmp( initOption, 'normalRandom')
        for i=1:n,
          Sol(i,:)=snapToBounds( randn(1,d), lb, ub); % snap to hypercube
          Fitness(i)=Fun(Sol(i,:));
        end
    elseif strcmp( initOption, 'uniformRandom')
        for i=1:n,
          Sol(i,:)= rand(1,d) * scale + lb;
          Fitness(i)=Fun(Sol(i,:));
        end
    end
    
    % Find the current best
    [fmin,I]=min(Fitness);
    best=Sol(I,:);

    % ======================================================  %
    % Note: As this is a demo, here we did not implement the  %
    % reduction of loudness and increase of emission rates.   %
    % Interested readers can do some parametric studies       %
    % and also implementation various changes of A and r etc  %
    % ======================================================  %

    % Start the iterations -- Bat Algorithm
    for i_ter=1:N_iter,
            % Loop over all bats/solutions
            for i=1:n,
              Q(i)=Qmin+(Qmin-Qmax)*rand;
              v(i,:)=v(i,:)+(Sol(i,:)-best)*Q(i);
              S(i,:)=Sol(i,:)+v(i,:);
              % Pulse rate
              if rand>r
                  S(i,:)=best+0.01*randn(1,d);
              end

              S(i,:) = snapToBounds( S(i,:), lb, ub); % snap to hypercube
              
         % Evaluate new solutions
               Fnew=Fun(S(i,:));
         % If the solution improves or not too loudness
               if (Fnew<=Fitness(i)) & (rand<A) ,
                    Sol(i,:)=S(i,:);
                    Fitness(i)=Fnew;
               end

              % Update the current best
              if Fnew<=fmin,
                    best=S(i,:);
                    fmin=Fnew;
              end
            end

    end

    
end

% 
% The only meaningful modification we made to the Bat algorithm is 
% the consideration of constraints. Here, if a particle leaves the 
% constrained area, it is snapped back to the boundary.
%
% If this is done before function evaluations, the particles should have
% no information of the function outside of the constrained hypercube.
%
function newPosn = snapToBounds( posn, lb, ub)
    for i = 1: length( posn)
        if posn( i) < lb
            posn( i) = lb;
        elseif posn( i) > ub
            posn( i) = ub;
        end
    end
    
    newPosn = posn;
end