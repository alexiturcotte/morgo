% 
% Note: part of the RGO and MORGO algorithm.
%
% Lateral root growth computation.
%
% Beta is a multiplier in the growth function.
%
function b = beta( numDims)
    % initialize vector of random numbers, dimension specified by 
    % the number of dimensions in the independent variable within
    % RGO/MORGO.
	yi = rand( 1, numDims);

    % compute and return b. See RGO paper for details.
	b = yi./sqrt(yi*yi'); 
end