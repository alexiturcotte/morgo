%
% Note: part of the RGO and MORGO algorithm.
%
% Necessary since MORGO and RGO are considering matrices, and some 
% Matlab methods aren't totally transparent when dealing with matrices.
%
% Computes the size of the first row of data in aList, as long as aList
% has some elements in it.
%
function [ theLength ] = bologna_length_fun( aList )

    if ( size( aList) == [0, 0])
        theLength = 0;
    else
        theLength = length( aList( 1,:));
    end

end

