% Note: part of the RGO and MORGO algorithm.
%
% Part of one of the root computations. See RGO paper for details.
%
% Parameters:
%
%   f               fitness value of original root apex
%   fmin            worst fitness in this generation
%   fmax            best fitness in this generation
%   smax and smin   given constants, 3 and 1 respectively (see paper)
%
function wi = branchingOp( f, fmin, fmax, smin, smax)
	wi = ((f - fmin)/(fmax - fmin)) * (smax - smin) + smin;
end
