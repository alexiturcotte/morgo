% 
% Note: part of the RGO and MORGO algorithm.
%
% Part of one of the root computations. See RGO paper for details.
%
% Parameters:
%
%   alpha           control parameter (1 in the paper)
%   sigloc_f        a little unclear, but this value is calculated via
%                   another function, and passed to this one
%   fmax, fmin      maxes and mins, as expected
%   wi              branching op value, calculated in another function
%
function w_i = inhibitionMech( alpha, sigloc_f, fmax, fmin, wi)
	w_i = alpha * (1- (sigloc_f / (fmax - fmin))) * wi;
end