%
% Script to load all functions. Called by moTATestSuite and MOTTestSuite.
%
% All functions are fairly standard, we implemented them based on their
% specifications in:
%
% Chase: ?A benchmark study of multi-objective optimization methods.? (ZDT)
% He:    ?Root growth optimizer with self-similar propagation? (Others)
%

% Simple 2D Functions

D = 2;

% basic function
basic = @( x) x( 1);

% sphere
sphere = @( x) sum( x.^2);

% schwefel 1.2
schwefel12 = @( x) sum(arrayfun(@(t) sum( x(1:t))^2, 1:D));

% schwefel 2.22
schwefel222 = @( x) sum( abs( x)) + prod( abs( x));

% rosenbrock
rosenbrock = @( x) 100*( x( 2) - x( 1)^2)^2 + (1 - x( 1))^2;

% rastrigin
rastrigin = @( x) sum( x.^2 - 10*cos( 2 * pi * x) + 10);

% schwefel
schwefel = @( x) 418.9829*D + sum( -x*sin( abs( x).^(1/2)));

% ackley
ackley = @( x) 20 + exp( 1) - 20*exp( -0.2 * sqrt(1/D * sum( x.^2))) - exp(1/D * sum( cos( 2 * pi * x)));

% griewank
griewank = @( x) 1/4000 * sum( x.^2) - prod( cos( x./(1:length( x)))) + 1;

% 2D Function Packs
Schwefel12Pack{1} = basic;
Schwefel12Pack{2} = schwefel12;
Schwefel12LB = -100;
Schwefel12UB =  100;

RosenbrockPack{1} = basic;
RosenbrockPack{2} = rosenbrock;
RosenbrockLB = -30;
RosenbrockUB =  30;

AckleyPack{1} = basic;
AckleyPack{2} = ackley;
AckleyLB = -32;
AckleyUB =  32;

% 
% Other Function Packs
% w/ exact values for Pareto Front
%

% Schaffer's Min-Min
f1 = @( x) x^2;
f2 = @( x) (x-2)^2;

SMM{1} = f1;
SMM{2} = f2;

numDimSMM = 1;

% ZDT1
ZDT1f1 = @( x) x( 1);
n = 30; 
ZDT1g  = @( x) 1 + 9*sum( x( 2:n))/(n - 1);
ZDT1f2 = @( x) ZDT1g( x) * ( 1 - sqrt( x(1) / ZDT1g( x)));

ZDT1FrontFun = @( x) 1 - sqrt( x( 1));

ZDT1{1} = ZDT1f1;
ZDT1{2} = ZDT1f2;

ZDT1LB = 0;
ZDT1UB = 1;

% ZDT1A
ZDT1Af1 = @( x) 10 * x( 1);
n = 30; 
ZDT1Ag  = @( x) 1 + 9*sum( x( 2:n))/(n - 1);
ZDT1Af2 = @( x) 10 * (ZDT1Ag( x) * ( 1 - sqrt( x(1) / ZDT1Ag( x))));

ZDT1AFrontFun = @( x) 10 * (1 - sqrt( x( 1))); %?

ZDT1A{1} = ZDT1Af1;
ZDT1A{2} = ZDT1Af2;

ZDT1ALB = 0;
ZDT1AUB = 10;

% ZDT2
ZDT2f1 = @( x) x( 1);
ZDT2g  = @( x) 1 + 9*sum( x( 2:n))/(n - 1);
ZDT2f2 = @( x) ZDT2g( x) * ( 1 - ( x( 1)/ZDT2g( x))^2);

ZDT2FrontFun = @( x) 1 - x( 1)^2;

ZDT2{1} = ZDT2f1;
ZDT2{2} = ZDT2f2;

ZDT2LB = 0;
ZDT2UB = 1;

% ZDT3
ZDT3f1 = @( x) x( 1);
ZDT3g  = @( x) 1 + 9*sum( x( 2:n))/(n - 1);
ZDT3f2 = @( x) ZDT3g( x) * ( 1 - sqrt( x( 1)/ZDT3g( x)) - x( 1)/ZDT3g( x) * sin( 10 * pi * x( 1)));

ZDT3FrontFun = @( x) 1 - sqrt( x(1)) - x( 1) * sin( 10 * pi * x( 1));

ZDT3{1} = ZDT3f1;
ZDT3{2} = ZDT3f2;

ZDT3LB = 0;
ZDT3UB = 1;

% ZDT{1, 2, 3} dims
ZDT1Dim  = n;
ZDT1ADim = n;
ZDT2Dim  = n;
ZDT3Dim  = n;

% LZ Function
d = 5;
LZDim = d;
JOdd = 1:2:LZDim;
JEven = 2:2:LZDim;
LZf1 = @( x) x( 1) + 2/length( JOdd) * sum( (x( JOdd) - sin( 6 * pi * x( 1) + JOdd * pi/d)).^2);
LZf2 = @( x) 1 - sqrt( x( 1)) + 2/length( JEven) * sum( (x( JEven) - sin( 6 * pi * x( 1) + JEven * pi/d)).^2);
LZ{1} = LZf1;
LZ{2} = LZf2;

% ZDT4
ZDT4f1 = @( x) x( 1);
ZDT4g  = @( x) 1 + 10 * (n - 1) + sum( x( 2:n).^2 - 10 * cos( 4 * pi * x( 2:n)));
ZDT4f2 = @( x) ZDT4g( x) * ( 1 - sqrt( x( 1)/ZDT4g( x)));

ZDT4FrontFun = @( x) 1 - sqrt( x( 1));

ZDT4{1} = ZDT4f1;
ZDT4{2} = ZDT4f2;
ZDT4Dim = n;

ZDT4LB =  0;
ZDT4UB =  5;

% ZDT6
n = 10;
ZDT6f1 = @( x) 1 - exp( -4 * x( 1)) * sin( 6 * pi * x( 1))^6;
ZDT6g  = @( x) 1 + 9 * ( sum( x( 2:length( x)))/(length( x) - 1))^(1/4);
ZDT6f2 = @( x) ZDT6g( x) * (1 - ( ZDT6f1( x)/ZDT6g( x))^2);

ZDT6FrontFun = @( x) ( 1 - ZDT6f1( x)^2);

ZDT6{1} = ZDT6f1;
ZDT6{2} = ZDT6f2;
ZDT6Dim = n;

ZDT6LB = 0;
ZDT6UB = 1;


