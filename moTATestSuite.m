%% Test Suite for functions where Pareto Front is known.
%
%  Known Pareto Test Suite. Metrics are:
%
%   <> Error    sum squared errors from real Front.
%   <> Spread   standard deviation of points.
%   <> Timing   tic ... toc Matlab timing.
%
%  Needs the loadFunctions.m script to function properly.
%
%  Results will be available in the following:
%
%   runTimesZDT{1, 2, 3, 4, 6}                  run times.
%   averageDistanceToCurveZDT{1, 2, 3, 4, 6}    errors.
%   stdDevsF1Z{1, 2, 3, 4, 6}                   std for F1.
%   stdDevsF2Z{1, 2, 3, 4, 6}                   std for F2.
%
%   You can safely take the elementwise mean of stdDevF1 and stdDevF2
%   to obtain the spread metric.
%
% Parameters can be tweaked in the Initializations section if desired.
%
%% Initializations
%
% clear console for output
clc;

disp('Initializing...');

% load all functions from loadFunctions.m
loadFunctions;

% Test Functions are:
% ZDT{1,2,3,4,6}
% MO Algorithms: 
% mopso_fun, morgo_nodraw, morgo_paretoapprox_nodraw, mobats, mo_firefly

% For this test suite, run ...
numIters = 100;
% iterations on all algorithms.

% Algorithm Parameters:
mopsoParticles = 50;
morgoApices    = 10;
fliesParticles = 50;
batsParetoRes  = 40;
morgParetoRes  = 40;
flieParetoRes  = 40;
morgoMaxPareto = 40;
mopsoMaxPareto = 40;
morgoParticles = 50;

numReplicates = 20;

%% ZDT1

% build the Pareto Front Approximation
% resolution of 100
realParetoZDT1 = zeros( 2, 100);
for i = 0:0.01:1
    realParetoZDT1(1, floor( i*100 + 1)) = ZDT1f1( i); 
    realParetoZDT1(2, floor( i*100 + 1)) = ZDT1FrontFun( i); 
end

realParetoZDT1 = realParetoZDT1';

runTimesZDT1 = zeros( 5, numReplicates);
averageDistanceToCurveZDT1 = zeros( 5, numReplicates);
stdDevsF1Z1 = zeros( 5, numReplicates);
stdDevsF2Z1 = zeros( 5, numReplicates);

for i = 1 : numReplicates

    disp('Beginning: ZDT1 Test.');

    disp('ZDT1: mopso_fun test.');
    
    tic;
      mopsoRetrnZ1               = mopso_fun(                 ZDT1, ZDT1Dim, ZDT1LB, ZDT1UB, mopsoParticles, mopsoMaxPareto, numIters, 'uniformRandom');
    runTimesZDT1( 1, i) = toc;
    
    mopsoEvalsZ1 = [mopsoRetrnZ1.Cost];  
    
    [ ~, dist, ~] = distance2curve( realParetoZDT1, mopsoEvalsZ1');
    averageDistanceToCurveZDT1( 1, i) = mean( dist);
    stdDevsF1Z1( 1, i) = std( mopsoEvalsZ1( 1, :));
    stdDevsF2Z1( 1, i) = std( mopsoEvalsZ1( 2, :));

    disp('ZDT1: morgo_nodraw test.');
    
    tic;
    [ morgoEvalsZ1, morgoPosnsZ1 ] = morgo_nodraw(              ZDT1, ZDT1Dim, 'min', morgoApices, ZDT1LB, ZDT1UB, morgoParticles, morgoMaxPareto, numIters, 'uniformRandom');
    runTimesZDT1( 2, i) = toc;
    
    [ ~, dist, ~] = distance2curve( realParetoZDT1, morgoEvalsZ1');
    averageDistanceToCurveZDT1( 2, i) = mean( dist);
    stdDevsF1Z1( 2, i) = std( morgoEvalsZ1( 1, :));
    stdDevsF2Z1( 2, i) = std( morgoEvalsZ1( 2, :));
    
    disp('ZDT1: morgo_paretoapprox_nodraw test.');
    
    tic;
    [ morApEvalsZ1               ] = morgo_paretoapprox_nodraw( ZDT1, ZDT1Dim, 'min', morgoApices, ZDT1LB, ZDT1UB, morgParetoRes, numIters, 'uniformRandom');
    runTimesZDT1( 3, i) = toc;
    
    [ ~, dist, ~] = distance2curve( realParetoZDT1, morApEvalsZ1');
    averageDistanceToCurveZDT1( 3, i) = mean( dist);
    stdDevsF1Z1( 3, i) = std( morApEvalsZ1( 1, :));
    stdDevsF2Z1( 3, i) = std( morApEvalsZ1( 2, :));
    
    disp('ZDT1: mo_firefly test.');
    
    tic;
    [ fliesEvalsZ1, fliesPosnsZ1 ] = mo_firefly(                ZDT1, ZDT1Dim, ZDT1LB, ZDT1UB, fliesParticles, flieParetoRes, numIters, 'uniformRandom');
    runTimesZDT1( 4, i) = toc;
    
    [ ~, dist, ~] = distance2curve( realParetoZDT1, fliesEvalsZ1');
    averageDistanceToCurveZDT1( 4, i) = mean( dist);
    stdDevsF1Z1( 4, i) = std( fliesEvalsZ1( 1, :));
    stdDevsF2Z1( 4, i) = std( fliesEvalsZ1( 2, :));
    
    disp('ZDT1: mobats test.'); 
    
    tic;
      mbatsEvalsZ1                 = mobats(                    ZDT1, ZDT1Dim, batsParetoRes, ZDT1LB, ZDT1UB, numIters, 'uniformRandom');
    runTimesZDT1( 5, i) = toc;

    [ ~, dist, ~] = distance2curve( realParetoZDT1, mbatsEvalsZ1');
    averageDistanceToCurveZDT1( 5, i) = mean( dist);
    stdDevsF1Z1( 5, i) = std( mbatsEvalsZ1( 1, :));
    stdDevsF2Z1( 5, i) = std( mbatsEvalsZ1( 2, :));
    
    disp('------------------------------');

end

%% ZDT2

% build the Pareto Front Approximation
% resolution of 100
realParetoZDT2 = zeros( 2, 100);
for i = 0:0.01:1
    realParetoZDT2(1, floor( i*100 + 1)) = ZDT2f1( i); 
    realParetoZDT2(2, floor( i*100 + 1)) = ZDT2FrontFun( i); 
end

realParetoZDT2 = realParetoZDT2';

runTimesZDT2 = zeros( 5, numReplicates);
averageDistanceToCurveZDT2 = zeros( 5, numReplicates);
stdDevsF1Z2 = zeros( 5, numReplicates);
stdDevsF2Z2 = zeros( 5, numReplicates);

for i = 1 : numReplicates

    disp('Beginning: ZDT2 Test.');

    disp('ZDT2: mopso_fun test.');
    
    tic;
      mopsoRetrnZ2               = mopso_fun(                 ZDT2, ZDT2Dim, ZDT2LB, ZDT2UB, mopsoParticles, mopsoMaxPareto, numIters, 'uniformRandom');
    runTimesZDT2( 1, i) = toc;
    
    mopsoEvalsZ2 = [mopsoRetrnZ2.Cost];  
    
    [ ~, dist, ~] = distance2curve( realParetoZDT2, mopsoEvalsZ2');
    averageDistanceToCurveZDT2( 1, i) = mean( dist);
    stdDevsF1Z2( 1, i) = std( mopsoEvalsZ2( 1, :));
    stdDevsF2Z2( 1, i) = std( mopsoEvalsZ2( 2, :));
    
    disp('ZDT2: morgo_nodraw test.');
    
    tic;
    [ morgoEvalsZ2, morgoPosnsZ2 ] = morgo_nodraw(              ZDT2, ZDT2Dim, 'min', morgoApices, ZDT2LB, ZDT2UB, morgoParticles, morgoMaxPareto, numIters, 'uniformRandom');
    runTimesZDT2( 2, i) = toc;
    
    [ ~, dist, ~] = distance2curve( realParetoZDT2, morgoEvalsZ2');
    averageDistanceToCurveZDT2( 2, i) = mean( dist);
    stdDevsF1Z2( 2, i) = std( morgoEvalsZ2( 1, :));
    stdDevsF2Z2( 2, i) = std( morgoEvalsZ2( 2, :));
    
    disp('ZDT2: morgo_paretoapprox_nodraw test.');
    
    tic;
    [ morApEvalsZ2               ] = morgo_paretoapprox_nodraw( ZDT2, ZDT2Dim, 'min', morgoApices, ZDT2LB, ZDT2UB, morgParetoRes, numIters, 'uniformRandom');
    runTimesZDT2( 3, i) = toc;
    
    [ ~, dist, ~] = distance2curve( realParetoZDT2, morApEvalsZ2');
    averageDistanceToCurveZDT2( 3, i) = mean( dist);
    stdDevsF1Z2( 3, i) = std( morApEvalsZ2( 1, :));
    stdDevsF2Z2( 3, i) = std( morApEvalsZ2( 2, :));
    
    disp('ZDT2: mo_firefly test.');
    
    tic;
    [ fliesEvalsZ2, fliesPosnsZ2 ] = mo_firefly(                ZDT2, ZDT2Dim, ZDT2LB, ZDT2UB, fliesParticles, flieParetoRes, numIters, 'uniformRandom');
    runTimesZDT2( 4, i) = toc;
    
    [ ~, dist, ~] = distance2curve( realParetoZDT2, fliesEvalsZ2');
    averageDistanceToCurveZDT2( 4, i) = mean( dist);
    stdDevsF1Z2( 4, i) = std( fliesEvalsZ2( 1, :));
    stdDevsF2Z2( 4, i) = std( fliesEvalsZ2( 2, :));
    
    disp('ZDT2: mobats test.');
    
    tic;
      mbatsEvalsZ2                 = mobats(                    ZDT2, ZDT2Dim, batsParetoRes, ZDT2LB, ZDT2UB, numIters, 'uniformRandom');
    runTimesZDT2( 5, i) = toc;

    [ ~, dist, ~] = distance2curve( realParetoZDT2, mbatsEvalsZ2');
    averageDistanceToCurveZDT2( 5, i) = mean( dist);
    stdDevsF1Z2( 5, i) = std( mbatsEvalsZ2( 1, :));
    stdDevsF2Z2( 5, i) = std( mbatsEvalsZ2( 2, :));
    
    disp('------------------------------');

end

%% ZDT3

% build the Pareto Front Approximation
% resolution of 100
realParetoZDT3 = zeros( 2, 100);
for i = 0:0.01:1
    realParetoZDT3(1, floor( i*100 + 1)) = ZDT3f1( i); 
    realParetoZDT3(2, floor( i*100 + 1)) = ZDT3FrontFun( i); 
end

realParetoZDT3 = realParetoZDT3';

runTimesZDT3 = zeros( 5, numReplicates);
averageDistanceToCurveZDT3 = zeros( 5, numReplicates);
stdDevsF1Z3 = zeros( 5, numReplicates);
stdDevsF2Z3 = zeros( 5, numReplicates);

for i = 1 : numReplicates

    disp('Beginning: ZDT3 Test.');

    disp('ZDT3: mopso_fun test.');
    
    tic;
      mopsoRetrnZ3               = mopso_fun(                 ZDT3, ZDT3Dim, ZDT3LB, ZDT3UB, mopsoParticles, mopsoMaxPareto, numIters, 'uniformRandom');
    runTimesZDT3( 1, i) = toc;
    
    mopsoEvalsZ3 = [mopsoRetrnZ3.Cost];  
    
    [ ~, dist, ~] = distance2curve( realParetoZDT3, mopsoEvalsZ3');
    averageDistanceToCurveZDT3( 1, i) = mean( dist);
    stdDevsF1Z3( 1, i) = std( mopsoEvalsZ3( 1, :));
    stdDevsF2Z3( 1, i) = std( mopsoEvalsZ3( 2, :));
    
    disp('ZDT3: morgo_nodraw test.');
    
    tic;
    [ morgoEvalsZ3, morgoPosnsZ3 ] = morgo_nodraw(              ZDT3, ZDT3Dim, 'min', morgoApices, ZDT3LB, ZDT3UB, morgoParticles, morgoMaxPareto, numIters, 'uniformRandom');
    runTimesZDT3( 2, i) = toc;
    
    [ ~, dist, ~] = distance2curve( realParetoZDT3, morgoEvalsZ3');
    averageDistanceToCurveZDT3( 2, i) = mean( dist);
    stdDevsF1Z3( 2, i) = std( morgoEvalsZ3( 1, :));
    stdDevsF2Z3( 2, i) = std( morgoEvalsZ3( 2, :));
    
    disp('ZDT3: morgo_paretoapprox_nodraw test.');
    
    tic;
    [ morApEvalsZ3               ] = morgo_paretoapprox_nodraw( ZDT3, ZDT3Dim, 'min', morgoApices, ZDT3LB, ZDT3UB, morgParetoRes, numIters, 'uniformRandom');
    runTimesZDT3( 3, i) = toc;
    
    [ ~, dist, ~] = distance2curve( realParetoZDT3, morApEvalsZ3');
    averageDistanceToCurveZDT3( 3, i) = mean( dist);
    stdDevsF1Z3( 3, i) = std( morApEvalsZ3( 1, :));
    stdDevsF2Z3( 3, i) = std( morApEvalsZ3( 2, :));
    
    disp('ZDT3: mo_firefly test.');
    
    tic;
    [ fliesEvalsZ3, fliesPosnsZ3 ] = mo_firefly(                ZDT3, ZDT3Dim, ZDT3LB, ZDT3UB, fliesParticles, flieParetoRes, numIters, 'uniformRandom');
    runTimesZDT3( 4, i) = toc;
    
    [ ~, dist, ~] = distance2curve( realParetoZDT3, fliesEvalsZ3');
    averageDistanceToCurveZDT3( 4, i) = mean( dist);
    stdDevsF1Z3( 4, i) = std( fliesEvalsZ3( 1, :));
    stdDevsF2Z3( 4, i) = std( fliesEvalsZ3( 2, :));
    
    disp('ZDT3: mobats test.');
    
    tic;
      mbatsEvalsZ3                 = mobats(                    ZDT3, ZDT3Dim, batsParetoRes, ZDT3LB, ZDT3UB, numIters, 'uniformRandom');
    runTimesZDT3( 5, i) = toc;

    [ ~, dist, ~] = distance2curve( realParetoZDT3, mbatsEvalsZ3');
    averageDistanceToCurveZDT3( 5, i) = mean( dist);
    stdDevsF1Z3( 5, i) = std( mbatsEvalsZ3( 1, :));
    stdDevsF2Z3( 5, i) = std( mbatsEvalsZ3( 2, :));
    
    disp('------------------------------');

end


%% ZDT4

% build the Pareto Front Approximation
% resolution of 100
realParetoZDT4 = zeros( 2, 100);
for i = 0:0.01:1
    realParetoZDT4(1, floor( i*100 + 1)) = ZDT4f1( i); 
    realParetoZDT4(2, floor( i*100 + 1)) = ZDT4FrontFun( i); 
end

realParetoZDT4 = realParetoZDT4';

runTimesZDT4 = zeros( 5, numReplicates);
averageDistanceToCurveZDT4 = zeros( 5, numReplicates);
stdDevsF1Z4 = zeros( 5, numReplicates);
stdDevsF2Z4 = zeros( 5, numReplicates);

for i = 1 : numReplicates

    disp('Beginning: ZDT4 Test.');

    disp('ZDT4: mopso_fun test.');
    
    tic;
      mopsoRetrnZ4               = mopso_fun(                 ZDT4, ZDT4Dim, ZDT4LB, ZDT4UB, mopsoParticles, mopsoMaxPareto, numIters, 'uniformRandom');
    runTimesZDT4( 1, i) = toc;
    
    mopsoEvalsZ4 = [mopsoRetrnZ4.Cost];  
    
    [ ~, dist, ~] = distance2curve( realParetoZDT4, mopsoEvalsZ4');
    averageDistanceToCurveZDT4( 1, i) = mean( dist);
    stdDevsF1Z4( 1, i) = std( mopsoEvalsZ4( 1, :));
    stdDevsF2Z4( 1, i) = std( mopsoEvalsZ4( 2, :));
    
    disp('ZDT4: morgo_nodraw test.');
    
    tic;
    [ morgoEvalsZ4, morgoPosnsZ4 ] = morgo_nodraw(              ZDT4, ZDT4Dim, 'min', morgoApices, ZDT4LB, ZDT4UB, morgoParticles, morgoMaxPareto, numIters, 'uniformRandom');
    runTimesZDT4( 2, i) = toc;
    
    [ ~, dist, ~] = distance2curve( realParetoZDT4, morgoEvalsZ4');
    averageDistanceToCurveZDT4( 2, i) = mean( dist);
    stdDevsF1Z4( 2, i) = std( morgoEvalsZ4( 1, :));
    stdDevsF2Z4( 2, i) = std( morgoEvalsZ4( 2, :));
    
    disp('ZDT4: morgo_paretoapprox_nodraw test.');
    
    tic;
    [ morApEvalsZ4               ] = morgo_paretoapprox_nodraw( ZDT4, ZDT4Dim, 'min', morgoApices, ZDT4LB, ZDT4UB, morgParetoRes, numIters, 'uniformRandom');
    runTimesZDT4( 3, i) = toc;
    
    [ ~, dist, ~] = distance2curve( realParetoZDT4, morApEvalsZ4');
    averageDistanceToCurveZDT4( 3, i) = mean( dist);
    stdDevsF1Z4( 3, i) = std( morApEvalsZ4( 1, :));
    stdDevsF2Z4( 3, i) = std( morApEvalsZ4( 2, :));
    
    disp('ZDT4: mo_firefly test.');
    
    tic;
    [ fliesEvalsZ4, fliesPosnsZ4 ] = mo_firefly(                ZDT4, ZDT4Dim, ZDT4LB, ZDT4UB, fliesParticles, flieParetoRes, numIters, 'uniformRandom');
    runTimesZDT4( 4, i) = toc;
    
    [ ~, dist, ~] = distance2curve( realParetoZDT4, fliesEvalsZ4');
    averageDistanceToCurveZDT4( 4, i) = mean( dist);
    stdDevsF1Z4( 4, i) = std( fliesEvalsZ4( 1, :));
    stdDevsF2Z4( 4, i) = std( fliesEvalsZ4( 2, :));
    
    disp('ZDT4: mobats test.'); 
    
    tic;
      mbatsEvalsZ4                 = mobats(                    ZDT4, ZDT4Dim, batsParetoRes, ZDT4LB, ZDT4UB, numIters, 'uniformRandom');
    runTimesZDT4( 5, i) = toc;

    [ ~, dist, ~] = distance2curve( realParetoZDT4, mbatsEvalsZ4');
    averageDistanceToCurveZDT4( 5, i) = mean( dist);
    stdDevsF1Z4( 5, i) = std( mbatsEvalsZ4( 1, :));
    stdDevsF2Z4( 5, i) = std( mbatsEvalsZ4( 2, :));
    
    disp('------------------------------');

end


%% ZDT6

% build the Pareto Front Approximation
% resolution of 100
realParetoZDT6 = zeros( 2, 100);
for i = 0:0.01:1
    realParetoZDT6(1, floor( i*100 + 1)) = ZDT6f1( i); 
    realParetoZDT6(2, floor( i*100 + 1)) = ZDT6FrontFun( i); 
end

realParetoZDT6 = realParetoZDT6';

runTimesZDT6 = zeros( 5, numReplicates);
averageDistanceToCurveZDT6 = zeros( 5, numReplicates);
stdDevsF1Z6 = zeros( 5, numReplicates);
stdDevsF2Z6 = zeros( 5, numReplicates);

for i = 1 : numReplicates

    disp('Beginning: ZDT6 Test.');

    disp('ZDT6: mopso_fun test.');
    
    tic;
      mopsoRetrnZ6               = mopso_fun(                 ZDT6, ZDT6Dim, ZDT6LB, ZDT6UB, mopsoParticles, mopsoMaxPareto, numIters, 'uniformRandom');
    runTimesZDT6( 1, i) = toc;
    
    mopsoEvalsZ6 = [mopsoRetrnZ6.Cost];  
    
    [ ~, dist, ~] = distance2curve( realParetoZDT6, mopsoEvalsZ6');
    averageDistanceToCurveZDT6( 1, i) = mean( dist);
    stdDevsF1Z6( 1, i) = std( mopsoEvalsZ6( 1, :));
    stdDevsF2Z6( 1, i) = std( mopsoEvalsZ6( 2, :));
    
    disp('ZDT6: morgo_nodraw test.');
    
    tic;
    [ morgoEvalsZ6, morgoPosnsZ6 ] = morgo_nodraw(              ZDT6, ZDT6Dim, 'min', morgoApices, ZDT6LB, ZDT6UB, morgoParticles, morgoMaxPareto, numIters, 'uniformRandom');
    runTimesZDT6( 2, i) = toc;
    
    [ ~, dist, ~] = distance2curve( realParetoZDT6, morgoEvalsZ6');
    averageDistanceToCurveZDT6( 2, i) = mean( dist);
    stdDevsF1Z6( 2, i) = std( morgoEvalsZ6( 1, :));
    stdDevsF2Z6( 2, i) = std( morgoEvalsZ6( 2, :));
    
    disp('ZDT6: morgo_paretoapprox_nodraw test.');
    
    tic;
    [ morApEvalsZ6               ] = morgo_paretoapprox_nodraw( ZDT6, ZDT6Dim, 'min', morgoApices, ZDT6LB, ZDT6UB, morgParetoRes, numIters, 'uniformRandom');
    runTimesZDT6( 3, i) = toc;
    
    [ ~, dist, ~] = distance2curve( realParetoZDT6, morApEvalsZ6');
    averageDistanceToCurveZDT6( 3, i) = mean( dist);
    stdDevsF1Z6( 3, i) = std( morApEvalsZ6( 1, :));
    stdDevsF2Z6( 3, i) = std( morApEvalsZ6( 2, :));
    
    disp('ZDT6: mo_firefly test.');
    
    tic;
    [ fliesEvalsZ6, fliesPosnsZ6 ] = mo_firefly(                ZDT6, ZDT6Dim, ZDT6LB, ZDT6UB, fliesParticles, flieParetoRes, numIters, 'uniformRandom');
    runTimesZDT6( 4, i) = toc;
    
    [ ~, dist, ~] = distance2curve( realParetoZDT6, fliesEvalsZ6');
    averageDistanceToCurveZDT6( 4, i) = mean( dist);
    stdDevsF1Z6( 4, i) = std( fliesEvalsZ6( 1, :));
    stdDevsF2Z6( 4, i) = std( fliesEvalsZ6( 2, :));
    
    disp('ZDT6: mobats test.');
    
    tic;
      mbatsEvalsZ6                 = mobats(                    ZDT6, ZDT6Dim, batsParetoRes, ZDT6LB, ZDT6UB, numIters, 'uniformRandom');
    runTimesZDT6( 5, i) = toc;

    [ ~, dist, ~] = distance2curve( realParetoZDT6, mbatsEvalsZ6');
    averageDistanceToCurveZDT6( 5, i) = mean( dist);
    stdDevsF1Z6( 5, i) = std( mbatsEvalsZ6( 1, :));
    stdDevsF2Z6( 5, i) = std( mbatsEvalsZ6( 2, :));
    
    disp('------------------------------');

end

%% END
disp('Done!');