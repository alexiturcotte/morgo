%% Test Suite for functions where Pareto Front is NOT known.
%
%  Unknown Pareto Test Suite. Metrics are:
%
%   <> Spread   standard deviation of points.
%   <> Timing   tic ... toc Matlab timing.
%
%  Needs the loadFunctions.m script to function properly.
%
%  Results will be available in the following:
%
%   runTimes{S12, Ros, Ack}     run times.
%   stdDevsF1{S12, Ros, Ack}    std for F1.
%   stdDevsF2{S12, Ros, Ack}    std for F2.
%
%   You can safely take the elementwise mean of stdDevF1 and stdDevF2
%   to obtain the spread metric.
%
% Parameters can be tweaked in the Initializations section if desired.
%
%% Initializations
%
% clear console for output
clc;

disp('Initializing...');

% load all functions from loadFunctions.m
loadFunctions;

% Test Functions are:
% Some Functions
% MO Algorithms: 
% mopso_fun, morgo_nodraw, morgo_paretoapprox_nodraw, mobats, mo_firefly

% For this test suite, run ...
numIters = 100;
% iterations on all algorithms.

% Algorithm Parameters:
mopsoParticles = 50;
morgoApices    = 10;
fliesParticles = 50;
batsParetoRes  = 40;
morgParetoRes  = 40;
flieParetoRes  = 40;
morgoMaxPareto = 40;
mopsoMaxPareto = 40;
morgoParticles = 50;

numReplicates = 20;

%% Schwefel 1.2
disp('Beginning: Schwefel 1.2 Test.');

runTimesS12 = zeros( 5, numReplicates);
stdDevsF1S12 = zeros( 5, numReplicates);
stdDevsF2S12 = zeros( 5, numReplicates);

for i = 1 : numReplicates

    disp('Schwefel 1.2: mopso_fun test.');
    
    tic;
      mopsoRetrnS12               = mopso_fun(                 Schwefel12Pack, D, Schwefel12LB, Schwefel12UB, mopsoParticles, mopsoMaxPareto, numIters, 'uniformRandom');
    runTimesS12( 1, i) = toc;
    
    mopsoEvalsS12 = [mopsoRetrnS12.Cost];  
    stdDevsF1S12( 1, i) = std( mopsoEvalsS12( 1, :));
    stdDevsF2S12( 1, i) = std( mopsoEvalsS12( 2, :));
    
    disp('Schwefel 1.2: morgo_nodraw test.');
    tic;
    [ morgoEvalsS12, morgoPosnsS12 ] = morgo_nodraw(              Schwefel12Pack, D, 'min', morgoApices, Schwefel12LB, Schwefel12UB, morgoParticles, morgoMaxPareto, numIters, 'uniformRandom');
    runTimesS12( 2, i) = toc;
    stdDevsF1S12( 2, i) = std( morgoEvalsS12( 1, :));
    stdDevsF2S12( 2, i) = std( morgoEvalsS12( 2, :));
    
    disp('Schwefel 1.2: morgo_paretoapprox_nodraw test.');
    tic;
    [ morApEvalsS12                ] = morgo_paretoapprox_nodraw( Schwefel12Pack, D, 'min', morgoApices, Schwefel12LB, Schwefel12UB, morgParetoRes, numIters, 'uniformRandom');
    runTimesS12( 3, i) = toc;
    stdDevsF1S12( 3, i) = std( morApEvalsS12( 1, :));
    stdDevsF2S12( 3, i) = std( morApEvalsS12( 2, :));
    
    disp('Schwefel 1.2: mo_firefly test.');
    tic;
    [ fliesEvalsS12, fliesPosnsS12 ] = mo_firefly(                Schwefel12Pack, D, Schwefel12LB, Schwefel12UB, fliesParticles, flieParetoRes, numIters, 'uniformRandom');
    runTimesS12( 4, i) = toc;
    stdDevsF1S12( 4, i) = std( fliesEvalsS12( 1, :));
    stdDevsF2S12( 4, i) = std( fliesEvalsS12( 2, :));
    
    disp('Schwefel 1.2: mobats test.');  
    tic;
      mbatsEvalsS12                  = mobats(                    Schwefel12Pack, D, batsParetoRes, Schwefel12LB, Schwefel12UB, numIters, 'uniformRandom');
    runTimesS12( 5, i) = toc;
    stdDevsF1S12( 5, i) = std( mbatsEvalsS12( 1, :));
    stdDevsF2S12( 5, i) = std( mbatsEvalsS12( 2, :));
end

disp( '--------------------');

%% Rosenbrock

disp('Beginning: Rosenbrock Test.');

runTimesRos = zeros( 5, numReplicates);
stdDevsF1Ros = zeros( 5, numReplicates);
stdDevsF2Ros = zeros( 5, numReplicates);

for i = 1 : numReplicates

    disp('Rosenbrock: mopso_fun test.');
    
    tic;
      mopsoRetrnRos               = mopso_fun(                 RosenbrockPack, D, RosenbrockLB, RosenbrockUB, mopsoParticles, mopsoMaxPareto, numIters, 'uniformRandom');
    runTimesRos( 1, i) = toc;
    
    mopsoEvalsRos = [mopsoRetrnRos.Cost];  
    stdDevsF1Ros( 1, i) = std( mopsoEvalsRos( 1, :));
    stdDevsF2Ros( 1, i) = std( mopsoEvalsRos( 2, :));
    
    disp('Rosenbrock: morgo_nodraw test.');
    tic;
    [ morgoEvalsRos, morgoPosnsRos ] = morgo_nodraw(              RosenbrockPack, D, 'min', morgoApices, RosenbrockLB, RosenbrockUB, morgoParticles, morgoMaxPareto, numIters, 'uniformRandom');
    runTimesRos( 2, i) = toc;
    stdDevsF1Ros( 2, i) = std( morgoEvalsRos( 1, :));
    stdDevsF2Ros( 2, i) = std( morgoEvalsRos( 2, :));
    
    disp('Rosenbrock: morgo_paretoapprox_nodraw test.');
    tic;
    [ morApEvalsRos                ] = morgo_paretoapprox_nodraw( RosenbrockPack, D, 'min', morgoApices, RosenbrockLB, RosenbrockUB, morgParetoRes, numIters, 'uniformRandom');
    runTimesRos( 3, i) = toc;
    stdDevsF1Ros( 3, i) = std( morApEvalsRos( 1, :));
    stdDevsF2Ros( 3, i) = std( morApEvalsRos( 2, :));
    
    disp('Rosenbrock: mo_firefly test.');
    tic;
    [ fliesEvalsRos, fliesPosnsRos ] = mo_firefly(                RosenbrockPack, D, RosenbrockLB, RosenbrockUB, fliesParticles, flieParetoRes, numIters, 'uniformRandom');
    runTimesRos( 4, i) = toc;
    stdDevsF1Ros( 4, i) = std( fliesEvalsRos( 1, :));
    stdDevsF2Ros( 4, i) = std( fliesEvalsRos( 2, :));
    
    disp('Rosenbrock: mobats test.');  
    tic;
      mbatsEvalsRos                  = mobats(                    RosenbrockPack, D, batsParetoRes, RosenbrockLB, RosenbrockUB, numIters, 'uniformRandom');
    runTimesRos( 5, i) = toc;
    stdDevsF1Ros( 5, i) = std( mbatsEvalsRos( 1, :));
    stdDevsF2Ros( 5, i) = std( mbatsEvalsRos( 2, :));
end

%% Ackley

disp('Beginning: Ackley Test.');

runTimesAck = zeros( 5, numReplicates);
stdDevsF1Ack = zeros( 5, numReplicates);
stdDevsF2Ack = zeros( 5, numReplicates);

for i = 1 : numReplicates

    disp('Ackley: mopso_fun test.');
    
    tic;
      mopsoRetrnAck               = mopso_fun(                 AckleyPack, D, AckleyLB, AckleyUB, mopsoParticles, mopsoMaxPareto, numIters, 'uniformRandom');
    runTimesAck( 1, i) = toc;
    
    mopsoEvalsAck = [mopsoRetrnAck.Cost];  
    stdDevsF1Ack( 1, i) = std( mopsoEvalsAck( 1, :));
    stdDevsF2Ack( 1, i) = std( mopsoEvalsAck( 2, :));
    
    disp('Ackley: morgo_nodraw test.');
    tic;
    [ morgoEvalsAck, morgoPosnsAck ] = morgo_nodraw(              AckleyPack, D, 'min', morgoApices, AckleyLB, AckleyUB, morgoParticles, morgoMaxPareto, numIters, 'uniformRandom');
    runTimesAck( 2, i) = toc;
    stdDevsF1Ack( 2, i) = std( morgoEvalsAck( 1, :));
    stdDevsF2Ack( 2, i) = std( morgoEvalsAck( 2, :));
    
    disp('Ackley: morgo_paretoapprox_nodraw test.');
    tic;
    [ morApEvalsAck                ] = morgo_paretoapprox_nodraw( AckleyPack, D, 'min', morgoApices, AckleyLB, AckleyUB, morgParetoRes, numIters, 'uniformRandom');
    runTimesAck( 3, i) = toc;
    stdDevsF1Ack( 3, i) = std( morApEvalsAck( 1, :));
    stdDevsF2Ack( 3, i) = std( morApEvalsAck( 2, :));
    
    disp('Ackley: mo_firefly test.');
    tic;
    [ fliesEvalsAck, fliesPosnsAck ] = mo_firefly(                AckleyPack, D, AckleyLB, AckleyUB, fliesParticles, flieParetoRes, numIters, 'uniformRandom');
    runTimesAck( 4, i) = toc;
    stdDevsF1Ack( 4, i) = std( fliesEvalsAck( 1, :));
    stdDevsF2Ack( 4, i) = std( fliesEvalsAck( 2, :));
    
    disp('Ackley: mobats test.');  
    tic;
      mbatsEvalsAck                  = mobats(                    AckleyPack, D, batsParetoRes, AckleyLB, AckleyUB, numIters, 'uniformRandom');
    runTimesAck( 5, i) = toc;
    stdDevsF1Ack( 5, i) = std( mbatsEvalsAck( 1, :));
    stdDevsF2Ack( 5, i) = std( mbatsEvalsAck( 2, :));
end
