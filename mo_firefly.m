%
% Yang's Firefly Algorithm for Multi-Objective Optimization
% 
% Implementation from Yang's Multi-Objective Firefly paper. 
%
% Parameters:
%
%   <> Funs             the functions to optimize
%   <> d                the dimension of the independent variable
%   <> lb               lower bound of constraint hypercube
%   <> ub               upper bound of constraint hypercube
%   <> nFlies           the number of fly particles
%   <> maxOnFront       max number of points on the Pareto Front to keep
%                       track of
%   <> nIter            max number of iterations
%   <> initOption       parameter to dictate which initialization scheme to
%                       use
% 
% Note: we assume that we are attempting to ~minimize~ the functions. In
%       order to adapt this to maximization, very little needs to be 
%       changed. In the meantime, one could simply minimize (-1) * Funs 
%       to find a maximum for Funs.
%
%       i.e. maximize Funs => minimize( - Funs)
%
function [ paretoFuns, paretoPosns ] = mo_firefly( Funs, d, lb, ub, nFlies, maxOnFront, nIter, initOption )

    numIters = nIter;   % number of iterations
    numFlies = nFlies;  % number of particles
    scale = ub - lb;    % scale - for initialization
    
    Posns = [];         % initialize Posns
    
    % uniform random initialization scheme
    if strcmp( initOption, 'uniformRandom')
        Posns = rand( d, numFlies) * scale + lb;
    end
    
    paretoPosns = Posns;
    paretoFuns = [];
    
    for q = 1: numIters
        
        for i = 1: numFlies
            
            foundSoln = false;
            
            % for each pair of flies (i, j)
            for j = 1: numFlies
                if i ~= j % if pair =/= (i, i)
                    
                    % evaluate obj functions for flies
                    iFlyVals = zeros( 1, length( Funs));
                    jFlyVals = zeros( 1, length( Funs));
                    for k = 1:length( Funs)
                        iFlyVals(k) = Funs{k}(Posns(:,i));
                        jFlyVals(k) = Funs{k}(Posns(:,j));
                    end
                    
                    % check for domination
                    if dominates( jFlyVals, iFlyVals, 'min')
                        
                        % if fly j dominates fly i, then
                        foundSoln = true;
                        
                        % move i towards j
                        beta0 = 1; 
                        gamma = 0.5/scale^2; % note: scale = ub - lb
                        
                        % see Yang's Firefly paper for details on these
                        % computations.
                        alpha = 0.01 * ones( d, 1) * scale;
                        beta = beta0 * exp(-gamma * (Posns(:,j) - Posns(:,1)).^2);
                        tmpf = alpha .* (rand( d, 1)-0.5) .* scale;
                        
                        % snap the new Posn to the constraint hypercube.
                        Posns(:,i) = snapToBounds( Posns(:,i).*(1-beta) + Posns(:,j) .* beta + tmpf, lb, ub);
                            
                    end
                end
            end
                    
            % if fly i is not dominated
            if ~ foundSoln
                
                % move randomly
                
                % generate array of random weights adding to 1
                w = zeros(1, length( Funs));
                top = 1;
                for z = 1: length( w)
                    if z == length( w)
                        w(z) = top;
                    else
                        rando = rand() * top;
                        w( z) = rando;
                        top = top - rando;
                    end
                end

                funEvals = zeros(1, numFlies);
                for z = 1: numFlies
                    for k = 1:length( Funs)
                         funEvals( z) = funEvals( z) + w(k) * Funs{k}(Posns(:,z));
                    end
                end

                minVal = min( funEvals);
                indexOfMin = find( funEvals == min( funEvals));
                indexOfMin = indexOfMin(1);
                alpha0 = 1;
                epsilon = rand( 1, d)';
                
                % snap to bounds if fly outside of bounding hypercube
                Posns(:, i) = snapToBounds( Posns( :, indexOfMin) + alpha0 * epsilon, lb, ub); % wow
            end   
        end
       
        if q == 1 % special consideration for the first iteration
            [paretoPosns, paretoFuns] = findNonDominated3( Funs, Posns, 'min');
        else
            % compute the Pareto Front thus far
            [paretoPosns, paretoFuns] = findNonDominated3( Funs, [Posns paretoPosns], 'min');
            
            % if the size of the front exceeds the max allowed, prune it.
            if ( size( paretoPosns, 2) > maxOnFront)
                
                % how many to prune?
                howMany = size( paretoPosns, 2) - maxOnFront;
                
                for k = 1: howMany
                    indexToRemove = randi( size( paretoPosns, 2));
                    paretoPosns( :, indexToRemove) = [];
                    paretoFuns( :, indexToRemove) = [];
                end
            end
        end
    end
end

% Determine if a dominated b w.r.t. a max or a min.
% Recall that a dominates b (in the min sense) if all a <= b and at least
% one a < b. Similar argument with >= and > instead of <= and < applies in 
% the max sense.
function doms = dominates( a, b, maxOrMin)
    if strcmp( maxOrMin, 'min')
        doms=all(a<=b) && any(a<b);
    else
        doms = all(a>=b) && any(a>b);
    end
end

% Simple function to evaluate all Funs at position posn.
function funEvals = evaluateFunctions( Funs, posn)
    funEvals = zeros( 1, length( Funs));
    for i = 1: length( funEvals)
        funEvals( i) = Funs{i}(posn);
    end
end

% Simple function to snap each of apex's values to be in [lb, ub].
% i.e. ensure that apex is within the hypercube defined by lb and ub.
function newApex = snapToBounds( apex, lb, ub)
    for i = 1: length( apex)
        if apex( i) < lb
            apex( i) = lb;
        elseif apex( i) > ub
            apex( i) = ub;
        end
    end
    newApex = apex;
end

% Big non-domination computation.
%
% Evaluates the functions Funs at all positions in Posns, and determines
% which of those positions are not dominated by any others (in the max or 
% min sense, indicated by the parameter maxOrMin).
function [nonDomPosns, nonDomFuns] = findNonDominated3( Funs, Posns, maxOrMin)
    nPop=size(Posns, 2);
    domList = zeros( 1, nPop);
    
    FunsList = zeros(length(Funs), size(Posns, 2));
    
    % evaluate all functions at all positions
    for i = 1 : size( FunsList, 2)
        FunsList( :, i) = evaluateFunctions( Funs, Posns( :, i));
    end
    
    for i=1:nPop-1
        for j=i+1:nPop
            
            % for each pair of positions, determine domination
            if dominates(FunsList(:,i),FunsList(:,j),maxOrMin)
               domList(j)=true;
            end
            
            if dominates(FunsList(:,j),FunsList(:,i),maxOrMin)
               domList(i)=true;
            end
            
        end
    end
    
    % remove all dominated positions
    Posns(:, find( domList == true)) = [];
    FunsList(:, find( domList == true)) = [];
    
    % return list of non dominated positions
    nonDomPosns = Posns;
    nonDomFuns = FunsList; % also return list of function evaluations, to 
                           % prevent wasteful future computations.
    
end

%
% Inefficient nonDomination computations, included for posterity.
%

% function [nonDomPosns, nonDomFuns] = findNonDominated( Funs, Posns)
%     index = 1;
%     nonDomPosns = [];
%     nonDomFuns = [];
%     % tic
%     for i = 1: size( Posns, 2)
%         bigDom = true; % i doms all j
%         for j = 1: size( Posns, 2)
%             if i ~= j
%                 iEval = evaluateFunctions( Funs, Posns( i, :));
%                 jEval = evaluateFunctions( Funs, Posns( j, :));
%                 if dominates( jEval, iEval, 'min')
%                     bigDom = false;
%                     break;
%                 end
%             end
%         end
%         if bigDom
%             nonDomPosns( index, :) = Posns( i, :);
%             nonDomFuns( index, :) = evaluateFunctions( Funs, Posns( i, :));
%             index = index + 1;
%         end
%     end
%     % toc
% end
% 
% function [nonDomPosns, nonDomFuns] = findNonDominated2( Funs, Posns)
%     index = 1;
%     nonDomPosns = [];
%     nonDomFuns = [];
%     si = 1;
%     while si <= size( Posns, 1)
%         addToSi = 1;
%         bigDom = true; % i doms all j
%         for j = 1: size( Posns, 1)
%             if si ~= j
%                 iEval = evaluateFunctions( Funs, Posns( si, :));
%                 jEval = evaluateFunctions( Funs, Posns(  j, :));
%                 if dominates( jEval, iEval, 'min')
%                     % here, i is dominated, so we can safely ignore it in
%                     % favour of j
%                     bigDom = false;
%                     Posns( si, :) = [];
%                     addToSi = 0;
%                     break;
%                 end
%             end
%         end
%         if bigDom
%             nonDomPosns( index, :) = Posns( si, :);
%             nonDomFuns( index, :) = evaluateFunctions( Funs, Posns( si, :));
%             index = index + 1;
%         end
%         si = si + addToSi;
%     end
% end
