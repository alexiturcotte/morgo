%
% Yang's Multi-Objective Bat Algorithm
%
% Takes functions from funList, builds a single-objective function by
% linear combination of these functions, and finds an optimum for that
% with the single-objective bat algorithm. 
%
% Parameters:
%
%   <> funList          the functions to optimize
%   <> d                the dimension of the independent variable
%   <> paretoRes        how many points to compute on the P. Front?
%   <> lb               lower bound of constraint hypercube
%   <> ub               upper bound of constraint hypercube
%   <> totalNumIters    max number of iterations
%   <> initOption       parameter to dictate which initialization scheme to
%                       use
%
function [ Q ] = mobats( funList, d, paretoRes, lb, ub, totalNumIters, initOption )

    % number of points to compute on the pareto front
    NPareto = paretoRes;
    
    % ensure that the call to the inner bats algorithm gets an opportunity
    % to optimize.
    innerNumIters = min( ceil( totalNumIters / NPareto), 50); 
    
    for k=1:NPareto,
        
        % Generate a weighting coefficient:w so that w1=w, w2=1-w, w1+w2=1.  
        % Observations suggest that systematically monotonic weights are
        % better than random weights.
        w = zeros( 1, length( funList));
        top = 1;
        for i = 1: length( w)
            if i == length( w)
                w( i) = top;
            else
                rando = rand() * top;
                w( i) = rando;
                top = top - rando;
            end
        end
        
        % randomize the array to ensure no statistical bias
        w = w( randperm( length( w)));
        
        % build the big function out of all functions in funList
        % note: linear combination w/ multiplication by random array w
        bigFun = @( x) 0;
        
        for i = 1: length( funList)
            thisFun = funList{ i};
            bigFun = @( x) bigFun( x) + w( i)*thisFun( x);
        end
        
        % compute a point on the P. Front w/ the single-objective bat alg.
        [best,~]=bat_algorithm([], bigFun, d, lb, ub, innerNumIters, initOption);
        
        % evaluate functions at that position
        objData = zeros( 1, length( funList));
        for i = 1: length( funList)
            objData( i) = funList{i}(best);
        end
        
        % add evaluation to the Pareto Front
        Q(k,:)=objData;
    end
    
    Q = Q';
    
end

