%
% Multi-Objective RGO
%
% Multi-Objective Optimization based on the Root Growth Optimization
% algorithm. 
%
% Parameters:
%
%   <> Funs             the functions to optimize
%   <> numDims          the dimension of the independent variable
%   <> maxOrMin         are we maximizing or minimizing the functions?
%   <> initNumApices    the initial number of root apices
%   <> lb               lower bound of constraint hypercube
%   <> ub               upper bound of constraint hypercube
%   <> maxParticles     max number of apices (huge performance loss here)
%   <> maxPareto        max number of points on the Pareto Front to keep
%                       track of
%   <> imax             max number of iterations
%   <> initOption       parameter to dictate which initialization scheme to
%                       use
%
function [ m, posn ] = morgo_nodraw( Funs, numDims, maxOrMin, initNumApices, lb, ub, maxParticles, maxPareto, imax, initOption )

    % Constants
    alpha = 1;              % control parameter for inhibition, given
    mainPcent = 0.30;       % given jesus angel di maria
    lateralPcent = 0.40;    % inferred
    killPcent = 0.25;       % how many aging roots to cull?
    numCells = 10;          % how many cells to divide hypercube?
    
    smax = 3; % given
    smin = 1; % given
    
    % more constants
    scale = ub - lb;        % to define area for initial dist
    
    % local learning constant, given
    l = 1;
    
    % Initializations, to avoid errors
    rootApices = zeros( numDims, initNumApices);
    rootFitness = zeros( length( Funs), initNumApices);
    
    maxNumRoots = maxParticles;
    
    % Steps:
    % Initialize the positions of the root apices
    % this is using uniform random distribution. Others are possible,
    % pending implementation.
    if strcmp( initOption, 'uniformRandom')
        for i = 1:initNumApices
            randArray = rand(1, numDims);
            initPos = randArray * scale + lb;
            rootApices( :, i) = initPos;
        end
    end
    
    % Calc fitness of values of each root apex
    for i = 1:initNumApices
        rootApex = rootApices( :, i);
        rootFitness( :, i) = evaluateFunctions( Funs, rootApex);
    end
    
    % pre main loop initialization 
    done = false;
    numIterations = 0;
    
    globalNonDominatedRoots = [];
    globalNonDominatedFitness = [];
    
    % While not meeting the termination cond, do:
    while ~ done
        
        % Div. root apices into main, lateral, aging
        
        rootApicesList = rootApices;
        rootApicesFitnessList = rootFitness;
        
        normieGridPosns = zeros( numDims, size( rootApicesList, 2));
        normieCellGrid = CreateGridMORGO( rootApicesList, numCells, 0.1);
        
        % build the grid of all positions
        for p = 1: size( normieGridPosns, 2)
            thisRoot = rootApicesList(:, p);
            for o = 1:numDims
                gridLB = normieCellGrid.LB;
                increment = gridLB(3) - gridLB(2);
                whereInGrid = floor( (thisRoot(o)-gridLB(2))/increment);
                if whereInGrid < 0
                    whereInGrid = 0;
                end
                normieGridPosns( o, p) = whereInGrid;
            end
        end
        
        % prune the grid, according to the maximum number of apices we
        % should have. Note that more apices can be created in this
        % iteration, so this isn't exactly a perfect implementation.
        [rootApicesList, rootApicesFitnessList, ~] = pruneGrid( rootApicesList, rootApicesFitnessList, normieGridPosns, maxNumRoots);
        
        % trim non-conforming (to constraints) root apices
        % from rootApicesToCheck.
        [rootApicesToCheck, rootApicesFitnessToCheck] = trimLBUB( rootApicesList, rootApicesFitnessList, lb, ub);
        
        % get non dominated apices from the valid root apices
        [nonDominatedPosns, nonDominatedFuns] = findNonDominated3( rootApicesFitnessToCheck, rootApicesToCheck, maxOrMin);
        
        % find globally non-dominated roots from the above apices, as well
        % as the changing pareto front.
        [globalNonDominatedRoots, globalNonDominatedFitness] = findNonDominated3( unique([globalNonDominatedFitness, nonDominatedFuns]', 'rows')', unique([globalNonDominatedRoots, nonDominatedPosns]', 'rows')', maxOrMin);
        
        % build the global grid of root apices.
        % this is used to guide the roots in their growth and split stages
        cellGrid = CreateGridMORGO( globalNonDominatedRoots, numCells, 0.1);
        
        globalGridPosns = zeros( numDims, size( globalNonDominatedRoots, 2));
        
        for p = 1: size( globalGridPosns, 2)
            thisRoot = globalNonDominatedRoots(:, p);
            for o = 1:numDims
                gridLB = cellGrid.LB;
                increment = gridLB(3) - gridLB(2);
                whereInGrid = floor( (thisRoot(o)-gridLB(2))/increment);
                if whereInGrid < 0
                    whereInGrid = 0;
                end
                globalGridPosns( o, p) = whereInGrid;
            end
        end
        
        % prune the globally non-dominated roots according to the max
        % allowed points on the pareto front
        [globalNonDominatedRoots, globalNonDominatedFitness, globalGridPosns] = pruneGrid( globalNonDominatedRoots, globalNonDominatedFitness, globalGridPosns, maxPareto);
        
        % save the number of apices we had considered at the end of the
        % last iteration
        numOfApicesLastTime = size( rootApicesList, 2);
        
        % determine the main roots
        mainRoots = zeros( numDims, floor( numOfApicesLastTime*mainPcent));
        mainRootsFitness = zeros( length( Funs), floor( numOfApicesLastTime*mainPcent));
        
        for i = 1: floor( numOfApicesLastTime*mainPcent)
            % chose a random root from among the best roots
            [mainRootsFitness( :, i), indexInNonDom] = roulette( nonDominatedFuns);
            locOfMax = find( ismember(rootApicesFitnessList', mainRootsFitness( :, i)', 'rows'), 1); % ***
            mainRoots( :, i) = rootApicesList( :, locOfMax( 1));
            
            % remove from list
            rootApicesList( :, locOfMax( 1)) = [];
            rootApicesFitnessList( :, locOfMax( 1)) = [];
            nonDominatedPosns( :, indexInNonDom) = [];
            nonDominatedFuns( :, indexInNonDom) = [];
            
            % restock the non-dominated roots if we run out
            if isempty( nonDominatedPosns)
                [nonDominatedPosns, nonDominatedFuns] = findNonDominated3( rootApicesFitnessList, rootApicesList, maxOrMin);
            end
        end
        
        % determine the lateral roots
        lateralRoots = zeros( numDims, floor( numOfApicesLastTime*lateralPcent));
        lateralRootsFitness = zeros( length( Funs), floor( numOfApicesLastTime*lateralPcent)); % r i p
        
        for i = 1:size( lateralRoots, 2)
            % pick a random root from the best possible ones
            [lateralRootsFitness( :, i), indexInNonDom] = roulette( nonDominatedFuns);
            findInMe = ismember(rootApicesFitnessList, lateralRootsFitness( :, i));
            locOfMax = find( findInMe(1,:), 1);
            lateralRoots( :, i) = rootApicesList( :, locOfMax( 1));
            
            % remove from list
            rootApicesList( :, locOfMax( 1)) = [];
            rootApicesFitnessList( :, locOfMax( 1)) = [];
            nonDominatedPosns( :, indexInNonDom) = [];
            nonDominatedFuns( :, indexInNonDom) = [];
            
            % restock non-dominated roots if needed
            if isempty( nonDominatedPosns)
                [nonDominatedPosns, nonDominatedFuns] = findNonDominated3( rootApicesFitnessList, rootApicesList, maxOrMin);
            end
        end
        
        % leftover roots are aging roots
        agingRoots = rootApicesList;
        agingRootsFitness = rootApicesFitnessList;
        
        % For each main root apex, do:
        
        % make array of random weights adding up to 1
        wat = zeros( 1, length( Funs));
        
        top = 1;
        for i = 1: length( wat)
            if i == length( wat)
                wat( i) = top;
            else
                rando = rand() * top;
                wat( i) = rando;
                top = top - rando;
            end
        end
        
        % randomly combine the fitnesses together. Here, we do this to 
        % compute fmin and fmax, which are decidedly single values. This 
        % is a pretty arbitrary choice, but it worked out here.
        rlc = randLinearComb( rootFitness, wat);
        
        fmin = min( rlc);   % (*) min evaluation in rlc
        fmax = max( rlc);   % (*) max evaluation in rlc
        newApices = [];     % preallocation of these vectors led to issues
        newFitnesses = [];  %
        
        % for each of the main roots
        for q = 1:size(mainRoots,2)
            
            % Regrow w root regrowing op
            x_itlast = mainRoots( :, q);
            
            
            % select a "best" in hypercube
            x_lbest = [];
            if size( globalNonDominatedRoots, 2) == 1
                
                x_lbest = globalNonDominatedRoots(:, 1);
            else
                
               % find all unique grid positions
               uniquePosns = unique( globalGridPosns', 'rows')';
               
               % how many roots are there in each grid hypercube?
               howManyTimes = zeros( 1, size( uniquePosns, 2));
               for n = 1: size( uniquePosns, 2)
                   howManyTimes( n) = sum( ismember( globalGridPosns', uniquePosns(:,n)', 'rows'));
               end
            
                % Selection Probabilities
                bbeta = 2;
                P = exp( -bbeta*howManyTimes);
                P = P/sum( P);
                
                sci = RouletteWheelSelection( P);
               
                % Selected Cell
                sc = uniquePosns( :, sci);

                % Selected Cell Members
                SCM = globalNonDominatedRoots( :, find( ismember( globalGridPosns', sc', 'rows') == 1));
                
                % Selected Member Index
                smi = randi([1 size(SCM,2)]);

                % Selected Member & "Best" for our purposes
                x_lbest = SCM(:, smi);
                
            end
            
            % ensure that x_it is within the allowed bounds
            x_it = snapToBounds( regrow( x_itlast, x_lbest, l), lb, ub);
            
            % Branch w root branching op
            % Eval the fitness of new root apices
            
            % how many to branch
            wi = branchingOp( wat*mainRootsFitness( :, q), fmin, fmax, smin, smax);
            
            % stdev of branches
            siginit = 1;    % (*) constant, see paper
            sigfin = 1;     % (*) as above
            n = 1;          % (*) as above
            sigi = stndDev( imax, q, siginit, sigfin, n);
            
            % for each time we should branch
            for w = 1: wi
                % normrnd gives the new position
                % snap it to hypercube bounds if need be
                newPos = snapToBounds( normrnd( x_it, sigi.^2)*l, lb, ub);
                newApices( :, bologna_length_fun( newApices) + 1) = newPos;
                newFitnesses( :, size( newFitnesses,2) + 1) = evaluateFunctions( Funs, newPos);
            end
            
            % Implement inhibition mech. of plant hormones
            % these calculations are part of this implementation. See paper
            % for details.
            sigloc_f = std( randLinearComb( newFitnesses, wat ));
            
            % compute how many to inhibit
            w_i = floor( inhibitionMech( alpha, sigloc_f, fmax, fmin, wi));
            
            % for how many we want to inhibit, inhibit them.
            for w = 1: w_i
                index = randi( bologna_length_fun( newApices), 1);
                newApices( :, index) = [];
                newFitnesses(:,  index) = [];
            end
            
            % add the regrowth parameters last, so they dont accidentally
            % get pruned
            newApices( :, bologna_length_fun( newApices) + 1) = x_it;
            newFitnesses( :, size( newFitnesses,2) + 1) = evaluateFunctions( Funs, x_it);
            
        % end For.
        end
        
        % For each lateral root apex, do:
        for q = 1: size( lateralRoots,2)
            x_itlast = lateralRoots( :, q);
            
            % Produce a new apex replacing original
            b = beta( numDims);
            x_it = snapToBounds( x_itlast + rand()*l*b', lb, ub);
            x_it_fit = evaluateFunctions( Funs, x_it);
            
            newApices( :, bologna_length_fun( newApices) + 1) = x_it;
            newFitnesses( :, size( newFitnesses,2) + 1) = x_it_fit;
            
        % end For.
        end
        
        % Implement shrinkage operator.
        % for now, arbitrarily kill the bottom 25% of aging roots
        
        % randomly permute them to ensure no bias
        shuffleIndices = randperm( floor( size( agingRoots,2)*killPcent));
        
        agingRoots( :, shuffleIndices) = [];
        agingRootsFitness( :, shuffleIndices) = [];
        
        % Rank root apices and label elite roots
        % ranking and labeling done on new iteration
        rootApices = [newApices, agingRoots];
        rootFitness = [newFitnesses, agingRootsFitness];
        
        if numIterations > imax
            done = true;
        end
        
        numIterations = numIterations + 1;
    % end While.
    end
    
    % Process results
    % trim non-conforming (to constraints) root apices
    [rootApicesToCheck, rootApicesFitnessToCheck] = trimLBUB( rootApices, rootFitness, lb, ub);

    % get non dominated apices from the valid root apices
    [nonDominatedPosns, nonDominatedFuns] = findNonDominated3( rootApicesFitnessToCheck, rootApicesToCheck, maxOrMin);

    % find globally non-dominated roots from the above apices, as well
    % as the changing pareto front.
    [globalNonDominatedRoots, globalNonDominatedFitness] = findNonDominated3( unique([globalNonDominatedFitness, nonDominatedFuns]', 'rows')', unique([globalNonDominatedRoots, nonDominatedPosns]', 'rows')', maxOrMin);
    
    m = globalNonDominatedFitness;
    posn = globalNonDominatedRoots;
end

% Determine if a dominated b w.r.t. a max or a min.
% Recall that a dominates b (in the min sense) if all a <= b and at least
% one a < b. Similar argument with >= and > instead of <= and < applies in 
% the max sense.
function doms = dominates( a, b, maxOrMin)
    if strcmp( maxOrMin, 'min')
        doms=all(a<=b) && any(a<b);
    else
        doms = all(a>=b) && any(a>b);
    end
end

% Big non-domination computation.
%
% Evaluates the functions Funs at all positions in Posns, and determines
% which of those positions are not dominated by any others (in the max or 
% min sense, indicated by the parameter maxOrMin).
function [nonDomPosns, nonDomFuns] = findNonDominated3( FunsList, Posns, maxOrMin)
    nPop=size(Posns, 2);
    domList = zeros( 1, nPop);
    
    for i=1:nPop-1
        for j=i+1:nPop
            
            if dominates(FunsList(:,i),FunsList(:,j),maxOrMin)
               domList(j)=true;
            end
            
            if dominates(FunsList(:,j),FunsList(:,i),maxOrMin)
               domList(i)=true;
            end
            
        end
    end
    
    Posns(:, find( domList == true)) = [];
    FunsList(:, find( domList == true)) = [];
    nonDomPosns = Posns;
    nonDomFuns = FunsList;
    
end

% Simple function to evaluate all Funs at position posn.
function funEvals = evaluateFunctions( Funs, posn)
    funEvals = zeros( 1, length( Funs));
    for i = 1: length( funEvals)
        funEvals( i) = Funs{i}(posn);
    end
end

% Function to chose a random element in theList
function [value, index] = roulette( theList)
    index = randi( size( theList, 2));
    value = theList( :, index);
end

% Function produces the linear combination of elements in A with weights w.
function spookyArray = randLinearComb( A, w)
    spookyArray = zeros( 1, size(A,2));
    
    for p = 1: size( A,2)
        spookyArray( p) = sum( A(:,p) .* w');
    end
end

% Function used to look in a grid to pick an appropriate element.
function i=RouletteWheelSelection(P)

    r=rand;
    
    C=cumsum(P);
    
    i=find(r<=C,1,'first');
end

% Simple function, snaps apex s.t. all elements are in [lb, ub].
function newApex = snapToBounds( apex, lb, ub)
    for i = 1: length( apex)
        if apex( i) < lb
            apex( i) = lb;
        elseif apex( i) > ub
            apex( i) = ub;
        end
    end
    newApex = apex;
end

% Remove elements in the grid (as well as posns and evals) so that they do
% not exceed the maxApices count, but do so in an intelligent way, pruing
% where there are many apices already.
function [gndp, gnde, grid] = pruneGrid( globalPosns, globalEvals, gridPosns, maxApices)
    
    gamma = 2;
    
    while size( globalPosns, 2) > maxApices
        
        uniquePosns = unique( gridPosns', 'rows')';

        howManyTimes = zeros( 1, size( uniquePosns, 2));
        for n = 1: size( uniquePosns, 2)
           howManyTimes( n) = sum( ismember( gridPosns', uniquePosns(:,n)', 'rows'));
        end

        % Selection Probabilities
        P = exp( gamma*howManyTimes);
        P = P/sum( P);

        sci = RouletteWheelSelection( P);

        % Selected Cell
        sc = uniquePosns( :, sci);

        % Selected Cell Members
        indicesWeWant = find( ismember( gridPosns', sc', 'rows') == 1);
        
        indexToDelete = indicesWeWant( randi( length( indicesWeWant)));
        
        globalPosns( :, indexToDelete) = [];
        globalEvals( :, indexToDelete) = [];
        gridPosns  ( :, indexToDelete) = [];
    end
    
    gndp = globalPosns;
    gnde = globalEvals;
    grid = gridPosns;
    
end

% Removes all apices which are stuck on the edges of the hypercube,
% increasing diversity of solutions.
function [trimmedMe, trimmedMyFitnesses] = trimLBUB( checkMe, checkMyFitnesses, lb, ub)
    trimmedMe = checkMe;
    trimmedMyFitnesses = checkMyFitnesses;
    removeIndices = [];
    for j = 1: size( checkMe, 2)
        for i = 1: size( checkMe, 1)
            if checkMe( i, j) == lb || checkMe( i, j) == ub
                removeIndices = [removeIndices j];
                break;
            end
        end
    end
    trimmedMe( :, removeIndices) = [];
    trimmedMyFitnesses( :, removeIndices) = [];
end        