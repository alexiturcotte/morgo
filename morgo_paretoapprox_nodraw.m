%
% Multi-Objective RGO w Naive Pareto Approximation
%
% Multi-Objective Optimization based on the Root Growth Optimization
% algorithm, using Yang's idea for how to compute the Pareto Front.
%
% Parameters:
%
%   <> Funs             the functions to optimize
%   <> numDims          the dimension of the independent variable
%   <> maxOrMin         are we maximizing or minimizing the functions?
%   <> initNumApices    the initial number of root apices
%   <> lb               lower bound of constraint hypercube
%   <> ub               upper bound of constraint hypercube
%   <> paretoRes        max number of points on the Pareto Front to keep
%                       track of
%   <> totalIter        max number of iterations
%   <> initOption       parameter to dictate which initialization scheme to
%                       use
%
function [ Q ] = morgo_paretoapprox_nodraw( Funs, numDims, maxOrMin, initNumApices, lb, ub, paretoRes, totalIter, initOption )

    % how many points do we compute?
    numOnFront   = paretoRes;
    
    % ensure that the inner call to RGO gets enough time to optimize.
    numIterInner = min( ceil( totalIter / numOnFront), 50);
    
    % for each point we wish to find on the pareto front, do
    for k = 1: numOnFront
        
        % generate weights summing to 1.
        w = zeros( 1, length( Funs));
        
        top = 1;
        for i = 1: length( w)
            if i == length( w)
                w( i) = top;
            else
                rando = rand() * top;
                w( i) = rando;
                top = top - rando;
            end
        end
        
        % linearly combine the functions with the weights into a big
        % function
        w = w( randperm( length( w)));
        bigFun = @( x) 0;
        
        for i = 1: length( Funs)
            thisFun = Funs{ i};
            bigFun = @( x) bigFun( x) + w( i)*thisFun( x);
        end
        
        % find optimum for that big function
        [~, posn] = rgo_nodraw(bigFun, numDims, maxOrMin, initNumApices, lb, ub, numIterInner, initOption);
        
        % evaluate all functions at the position
        objData = zeros( length( Funs), 1);
        for i = 1: length( Funs)
            val = Funs{i}(posn);
            objData( i) = val(1);
        end
        
        % add evals to pareto front
        Q(k,:)=objData;
        
    end
    
    Q = Q';
   
end