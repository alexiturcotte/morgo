function [  ] = plotAll( mopso, morgo, morap, flies, mbats, holdOrNo)

quickPlot( mopso, 'ro');
if strcmp( holdOrNo, 'hold');
    hold on;
end
quickPlot( morgo, 'bo');
quickPlot( morap, 'go');
quickPlot( flies, 'mo');
quickPlot( mbats, 'co');

end

