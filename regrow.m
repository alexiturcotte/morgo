% Note: part of the RGO and MORGO algorithm.
%
% Part of one of the root computations. See RGO paper for details.
%
% x_it          original pos of root apex this time
% x_itlast      original pos of root apex last time
% x_lbest       local best
% l             arbitrary parameter, equal to 1 (see paper)
function x_it = regrow( x_itlast, x_lbest, l )
    x_it = x_itlast + l * rand() * (x_lbest - x_itlast);
end

