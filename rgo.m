%
% RGO Method with Drawing 
%
% The RGO single-objective optimization algorithm is defined here, with the
% added feature of incremental visualization of results. 
%
% If you would like to use RGO without the visualization, please use
% rgo_nodraw.
%
% Parameters:
%
%   <>  F               the function you would like to optimize
%   <>  numDims         the number of dimensions of the ind. variable
%   <>  maxOrMin        are you maximizing or minimizing?
%   <>  initNumApices   how many apices to start with
%   <>  dimLimit        the scale of the computation - rgo optimizes in
%                       a [-dimLimit/2, dimLimit/2] area.
%
function [ m ] = rgo( F, numDims, maxOrMin, initNumApices, dimLimit )

    % for drawing
    pkg load geometry;
    figure; hold on; axis equal;

    % root growth optimize a given function F
    % produces the max/min value m
    % maxOrMin should be one of: 'max' or 'min'

    % note: maximize F == minimize -F
    if ( strcmp( maxOrMin, 'min'))
        F = @( x) -F( x);
    end
    
    % Constants
    l = 1;                  % local learning constant, given
    alpha = 1;              % control parameter for inhibition, given
    mainPcent = 0.30;       % how many roots to consider "main", given
    lateralPcent = 0.40;    % how many roots to consider "latera", inferred
    
    imax = 50;              % max # of iterations, up to the user
    
    smax = 3; % given, see paper.
    smin = 1; % given, see paper.
    
    scale = dimLimit; % to define area for initial distribution
    
    % Initializations
    rootApices = zeros( numDims, initNumApices);
    rootFitness = [];
    
    % Steps:
    % Initialize the positions of the root apices
    % (!) currently a uniform random distribution
    for i = 1:initNumApices
        randArray = rand(1, numDims);
        
        initPos = randArray * scale - scale/2;
        rootApices( :, i) = initPos;
    end
    
    % Calc fitness of values of each root apex
    for i = 1:initNumApices
        rootApex = rootApices( :, i);
        rootFitness( i) = F( rootApex);
    end
    
    % While not meeting the termination cond, do:
    done = false;
    numIterations = 0;
    
    while ~ done
        % Div. root apices into main, lateral, aging
        numOfApicesLastTime = length( rootApices);
        
        rootApicesList = rootApices;
        rootApicesFitnessList = rootFitness;
        
        mainRoots = zeros( numDims, floor( numOfApicesLastTime*mainPcent));
        mainRootsFitness = [];
        
        for i = 1:length( mainRoots)
            % select the most appropriate root
            mainRootsFitness( i) = max( rootApicesFitnessList);
            locOfMax = find( rootApicesFitnessList == max( rootApicesFitnessList));
            mainRoots( :, i) = rootApicesList( :, locOfMax( 1));

            % draw it
            drawPoint( mainRoots( :, i)', 'g.'); %'
            
            % remove from list
            rootApicesList( :, locOfMax( 1)) = [];
            rootApicesFitnessList( locOfMax( 1)) = [];
        end
        
        lateralRoots = zeros( numDims, floor( numOfApicesLastTime*lateralPcent));
        lateralRootsFitness = [];
        
        for i = 1:length( lateralRoots)
            % select the most appropriate root
            lateralRootsFitness( i) = max( rootApicesFitnessList);
            locOfMax = find( rootApicesFitnessList == max( rootApicesFitnessList));
            lateralRoots( :, i) = rootApicesList( locOfMax( 1));

            % draw it
            drawPoint( lateralRoots( :, i)', 'b.'); %' 
            
            % remove from list
            rootApicesList( :, locOfMax( 1)) = [];
            rootApicesFitnessList( locOfMax( 1)) = [];
        end
        
        % leftovers are aging roots
        agingRoots = rootApicesList;
        agingRootsFitness = rootApicesFitnessList;

        % draw them all
        for i = 1:length( agingRoots)
            drawPoint( agingRoots( :, i)', 'r.'); %'
        end
        
        % For each main root apex, do:
        fmin = min( rootFitness); 
        fmax = max( rootFitness); 
        newApices = []; 
        newFitnesses = []; 

        lineList = [];
        
        for q = 1:length(mainRoots)
            
            % Regrow w root regrowing op
            x_itlast = mainRoots( :, q);
            locOfBest = find( mainRootsFitness == max( mainRootsFitness));
            x_lbest = mainRoots( :, locOfBest( 1)); % best last time
            x_it = regrow( x_itlast, x_lbest, l);   % grow new root

            % draw roots growing
            % draw between x_itlast and x_it
            drawEdge([x_itlast' x_it']);
            drawPoint( x_it', 'g.'); %'
            drawnow();
            
            % Branch w root branching op
            % Eval the fitness of new root apices
            
            % how many to branch
            wi = branchingOp( mainRootsFitness( q), fmin, fmax, smin, smax);
            
            % stdev of branches
            siginit = 1;    % constant value, inferred
            sigfin = 1;     % constant value, inferred
            n = 1;          % constant value, inferred
            sigi = stndDev( imax, q, siginit, sigfin, n);
            
            % for each time we want to branch,
            for w = 1: wi
                % generate new root position w normrnd
                newPos = normrnd( x_it, sigi.^2);
                newApices( :, bologna_length_fun( newApices) + 1) = newPos;
                newFitnesses( length( newFitnesses) + 1) = F( newPos);

                % draw between x_it and newPos
                lineList( length( newFitnesses), :) = [x_it' newPos'];
                drawEdge([x_it' newPos']);
                drawPoint( newPos', 'g.'); %'
                drawnow();
            end
            
            % Implement inhibition mech. of plant hormones
            sigloc_f = std( newFitnesses); % calculate std of fitnesses
            w_i = floor( inhibitionMech( alpha, sigloc_f, fmax, fmin, wi));
            
            for w = 1: w_i
                % delete (this is inhibition)
                index = randi( bologna_length_fun( newApices), 1);
                newApices( :, index) = [];
                newFitnesses( index) = [];

                drawEdge(lineList( index, :), 'color', 'w');
                drawPoint( lineList( index, 3), lineList( index, 4), 'w.'); 
                drawnow();
                lineList(index, :) = [];
            end
            
            % add the regrowth parameters last, so they dont accidentally
            % get pruned
            newApices( :, bologna_length_fun( newApices) + 1) = x_it;
            newFitnesses( length( newFitnesses) + 1) = F( x_it);
            lineList( length( newFitnesses), :) = [x_itlast' x_it'];
            
        % end For.
        end
        
        % For each lateral root apex, do:
        for q = 1: length( lateralRoots)
            x_itlast = lateralRoots( :, q);
            
            % Produce a new apex replacing original
            b = beta( numDims);
            x_it = x_itlast + rand()*b'; %'
            x_it_fit = F( x_it);
            
            newApices( :, bologna_length_fun( newApices) + 1) = x_it;
            newFitnesses( length( newFitnesses) + 1) = x_it_fit;

            % draw between x_it and x_itlast
            drawEdge([x_itlast' x_it']);
            drawPoint( x_it', 'b.'); %'
            drawnow();
            
        % end For.
        end
        
        % Implement shrinkage operator.
        % i.e. arbitrarily kill the bottom 25% of aging roots
        runFor = length( agingRoots);
        for i = 1: floor( runFor*0.25)
            locOfMin = find( agingRootsFitness == min( agingRootsFitness));
            
            % remove from list
            drawPoint( agingRoots( :, locOfMin( 1))', 'k.'); %'
            agingRoots( :, locOfMin( 1)) = [];
            agingRootsFitness( locOfMin( 1)) = [];

            % draw figure out how to delete roots
        end
        
        % Rank root apices and label elite roots
        % ranking and elite tagging done at beginning of main loop, not
        % here
        rootApices = [newApices, agingRoots];
        rootFitness = [newFitnesses, agingRootsFitness];
        
        if numIterations > imax
            done = true;
        end
        
        numIterations = numIterations + 1;
    % end While.
    end
    
    % Process results
    m = max( rootFitness);
    
    if ( strcmp( maxOrMin, 'min'))
        m = -m;
    end
end