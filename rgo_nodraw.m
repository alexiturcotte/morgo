%
% RGO Method w/o Drawing 
%
% The RGO single-objective optimization algorithm is implemented here.
%
% If you would like to use RGO with the visualization, please use rgo.
%
% Parameters:
%
%   <>  F               the function you would like to optimize
%   <>  numDims         the number of dimensions of the ind. variable
%   <>  maxOrMin        are you maximizing or minimizing?
%   <>  initNumApices   how many apices to start with
%   <>  lb              lower bound of search space
%   <>  ub              upper bound of search space
%   <>  numIter         max # of iterations
%   <>  initOptions     selected initialization scheme
%
function [ m, posn ] = rgo_nodraw( F, numDims, maxOrMin, initNumApices, lb, ub, numIter, initOption )
    % root growth optimize a given function F
    % produces the max/min value m
    % maxOrMin should be one of: 'max' or 'min'

    if ( strcmp( maxOrMin, 'min'))
        F = @( x) -F( x);
    end
    
    % Constants
    l = 1;                      % local learning constant, given
    alpha = 1;                  % control parameter for inhibition, given
    mainPcent = 0.30;           % % of roots that are main, given
    lateralPcent = 0.40;        % % of roots that are lateral, inferred
    imax = numIter;
    
    smax = 3; % given, see paper.
    smin = 1; % given, see paper.
    
    scale = ub - lb; % to define area for initial distribution
    
    % Initializations
    rootApices = zeros( numDims, initNumApices);
    rootFitness = []; 
    
    % Steps:
    % Initialize the positions of the root apices
    % using the uniformRandom scheme
    if strcmp( initOption, 'uniformRandom')
        for i = 1:initNumApices
            randArray = rand(1, numDims);

            initPos = randArray * scale + lb;
            rootApices( :, i) = initPos;
        end
    end
    
    % Calc fitness of values of each root apex
    for i = 1:initNumApices
        rootApex = rootApices( :, i);
        rootFitness( i) = F( rootApex);
    end
    
    % While not meeting the termination cond, do:
    done = false;
    numIterations = 0;
    
    while ~ done
        % Div. root apices into main, lateral, aging
        numOfApicesLastTime = size( rootApices, 2);
        
        rootApicesList = rootApices;
        rootApicesFitnessList = rootFitness;
        
        % main roots
        
        mainRoots = zeros( numDims, floor( numOfApicesLastTime*mainPcent));
        mainRootsFitness = []; 
        
        for i = 1:size( mainRoots, 2)
            mainRootsFitness( i) = max( rootApicesFitnessList);
            locOfMax = find( rootApicesFitnessList == max( rootApicesFitnessList));
            mainRoots( :, i) = rootApicesList( :, locOfMax( 1));
            
            % remove from list
            rootApicesList( :, locOfMax( 1)) = [];
            rootApicesFitnessList( locOfMax( 1)) = [];
        end
        
        % lateral roots
        
        lateralRoots = zeros( numDims, floor( numOfApicesLastTime*lateralPcent));
        lateralRootsFitness = []; % r i p
        
        for i = 1:size( lateralRoots, 2)
            lateralRootsFitness( i) = max( rootApicesFitnessList);
            locOfMax = find( rootApicesFitnessList == max( rootApicesFitnessList));
            lateralRoots( :, i) = rootApicesList( locOfMax( 1));
            
            % remove from list
            rootApicesList( :, locOfMax( 1)) = [];
            rootApicesFitnessList( locOfMax( 1)) = [];
        end
        
        % aging roots
        
        agingRoots = rootApicesList;
        agingRootsFitness = rootApicesFitnessList;
        
        % For each main root apex, do:
        fmin = min( rootFitness); % min fitness value
        fmax = max( rootFitness); % max fitness value
        newApices = []; 
        newFitnesses = []; 
        
        % for each main root
        for q = 1:size( mainRoots, 2)
            
            % Regrow w root regrowing op
            x_itlast = mainRoots( :, q);
            locOfBest = find( mainRootsFitness == max( mainRootsFitness));
            x_lbest = mainRoots( :, locOfBest( 1)); % best last time
            x_it = snapToBounds( regrow( x_itlast, x_lbest, l), lb, ub);
            
            % Branch w root branching op
            % Eval the fitness of new root apices
            
            % how many to branch
            wi = ceil( branchingOp( mainRootsFitness( q), fmin, fmax, smin, smax));
            % stdev of branches
            siginit = 1;    % arbitrary constant.
            sigfin = 1;     % arbitrary constant.
            n = 1;          % arbitrary constant, see paper.
            % standard deviation calculation, as per the paper.
            sigi = stndDev( imax, q, siginit, sigfin, n); 
            
            % for each time we want to branch
            for w = 1: wi
                % get new root pos with normrnd
                newPos = snapToBounds( normrnd( x_it, sigi.^2), lb, ub);
                newApices( :, bologna_length_fun( newApices) + 1) = newPos;
                newFitnesses( length( newFitnesses) + 1) = F( newPos);
            end
            
            % Implement inhibition mech. of plant hormones
            sigloc_f = std( newFitnesses); 
            w_i = floor( inhibitionMech( alpha, sigloc_f, fmax, fmin, wi));
            
            % for each time we want to inhibit
            for w = 1: w_i
                % remove the root from consideration
                index = randi( bologna_length_fun( newApices), 1);
                newApices( :, index) = [];
                newFitnesses( index) = [];
            end
            
            % add the regrowth parameters last, so they dont accidentally
            % get pruned, kind of like the authors brain
            newApices( :, bologna_length_fun( newApices) + 1) = x_it;
            newFitnesses( length( newFitnesses) + 1) = F( x_it);
            
        % end For.
        end
        
        % For each lateral root apex, do:
        for q = 1: size( lateralRoots, 2)
            x_itlast = lateralRoots( :, q);
            
            % Produce a new apex replacing original
            b = beta( numDims);
            x_it = snapToBounds( x_itlast + rand()*b', lb, ub);
            x_it_fit = F( x_it);
            
            newApices( :, bologna_length_fun( newApices) + 1) = x_it;
            newFitnesses( length( newFitnesses) + 1) = x_it_fit;
            
        % end For.
        end
        
        % Implement shrinkage operator.
        % arbitrarily kill the bottom 25% of aging roots
        runFor = size( agingRoots, 2);
        for i = 1: floor( runFor*0.25)
            locOfMin = find( agingRootsFitness == min( agingRootsFitness));
            
            % remove from list
            agingRoots( :, locOfMin( 1)) = [];
            agingRootsFitness( locOfMin( 1)) = [];
        end
        
        % Rank root apices and label elite roots
        % note: ranking and elitism done at the beginning of the main loop,
        % through the main, lateral, aging classification process.
        rootApices = [newApices, agingRoots];
        rootFitness = [newFitnesses, agingRootsFitness];
        
        if numIterations > imax
            done = true;
        end
        
        numIterations = numIterations + 1;
    % end While.
    end
    
    % Process results
    m = max( rootFitness);
    posn = rootApices( :, find( rootFitness == m));
    
    if ( strcmp( maxOrMin, 'min'))
        m = -m;
    end
end

% Simple function, snaps an apex s.t. all position values are valid
% note: valid here means x \in [lb, ub]
function newApex = snapToBounds( apex, lb, ub)
    for i = 1: length( apex)
        if apex( i) < lb
            apex( i) = lb;
        elseif apex( i) > ub
            apex( i) = ub;
        end
    end
    
    newApex = apex;
end