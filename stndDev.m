%
% RGO Supporting Function
%
% See paper for specifics.
%
%   imax     max iteration number
%   i        current iteration number
%   siginit  initial stdev depending on value of searching range
%   sigfin   final stdev determined by expected accuracy standard 
%            in the program
function sigi = stndDev( imax, i, siginit, sigfin, n)
	sigi = (((imax - i)/imax)^n)*(siginit - sigfin) + sigfin;
end